# -*- encoding: utf8-*-

import requests
import json
import logging

from datetime import datetime
from bs4 import BeautifulSoup
from requests.adapters import HTTPAdapter
from libs import db_process
from libs.investment import stock_code, otc_history_price, se_history_price
from logging import debug
from pymongo import monitoring

class CommandLogger(monitoring.CommandListener):

    def started(self, event):
        debug("Command {0.command_name} with request id "
              "{0.request_id} started on server "
              "{0.connection_id}".format(event))

    def succeeded(self, event):
        debug("Command {0.command_name} with request id "
              "{0.request_id} on server {0.connection_id} "
              "succeeded in {0.duration_micros} "
              "microseconds".format(event))

    def failed(self, event):
        debug("Command {0.command_name} with request id "
              "{0.request_id} on server {0.connection_id} "
              "failed in {0.duration_micros} "
              "microseconds".format(event))

class MyFormatter(object):

    def format(self, record):
        return_msg = record.getMessage()

        if record.levelno == 20:
            return_msg = '\n[{}] {}'.format(
                datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
                return_msg
            )
        elif record.levelno == 40:
            return_msg = '[ERROR] ' + return_msg

        return return_msg

def log_process(log_name='test.log'):
    root = logging.getLogger()
    my_format = MyFormatter()

    fhdlr = logging.FileHandler(log_name, 'w')
    fhdlr.setFormatter(my_format)

    console = logging.StreamHandler()
    console.setFormatter(my_format)

    root.setLevel(logging.DEBUG)
    root.addHandler(console)
    root.addHandler(fhdlr)

    logging.getLogger('paramiko.transport').disabled = True

def get_all_stock():
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) '
                             'AppleWebKit/537.36 (KHTML, like Gecko) '
                             'Chrome/66.0.3359.181 Safari/537.36'}
    urls = [
        "https://isin.twse.com.tw/isin/C_public.jsp?strMode=1",
        "https://isin.twse.com.tw/isin/C_public.jsp?strMode=2",
        "https://isin.twse.com.tw/isin/C_public.jsp?strMode=3",
        "https://isin.twse.com.tw/isin/C_public.jsp?strMode=4",
        "https://isin.twse.com.tw/isin/C_public.jsp?strMode=5"
    ]

    all_stock = []
    session = requests.Session()
    session.mount('http://', HTTPAdapter(max_retries=3))

    for url in urls:
        IDNameNum, ISINNum, listingDateNum, marketTypeNum, industryNum, CFICodeNum = range(6)
        try:
            resp = session.get(url, headers=headers, timeout=5)
        except requests.exceptions.RequestException as e:
            print(e)
            continue
        #resp.encoding = 'utf_8'
        soup = BeautifulSoup(resp.text, 'html.parser')

        stock_list = soup.find(class_='h4').find_all('tr', {'align': None})
        if url[-1] == '1':
            marketTypeNum = -1
            industryNum = 3
            CFICodeNum = 4
        elif url[-1] == "3":
            marketTypeNum = 5
            industryNum = 6
            CFICodeNum = 7

        for stock in stock_list:
            stock_data = stock.find_all('td')

            if len(stock_data) <= 1:
                continue

            all_stock.append({
                'code': stock_data[IDNameNum].text.split()[0],
                'name': stock_data[IDNameNum].text.split()[1],
                'ISIN': stock_data[ISINNum].text,
                'listing_date': stock_data[listingDateNum].text,
                'market_type': '未上市' if marketTypeNum == -1 else stock_data[marketTypeNum].text,
                'industry': stock_data[industryNum].text,
                'CFI_code': stock_data[CFICodeNum].text
            })

    db = db_process.DBProcess()
    db.write_data("investment", stock_code, all_stock)

def str_to_float(target_str):
    return_str = target_str
    try:
        return_str = float(target_str)
    except ValueError:
        pass
    return return_str

def _date_translate(target_date, db=None):
    return f"{int(target_date[:4])-1911}/{target_date[4:6]}/{target_date[6:]}"

def get_emerging_stock_data(target_date, db=None):
    """
    Not finish
    """
    rsp = requests.get(f"https://www.tpex.org.tw/web/emergingstock/historical/daily/EMDaily_dl.php?l=zh-tw&f=EMdes010.{target_date}-C.csv")
    if rsp.status_code != 200:
        print("Can't get emerging stock data")
        return 1

    es_datas = []

    for line in rsp.text.split("\r\n"):
        data = [row.replace("\"", "").strip() for row in line.split(",")]
        if data[0] == "BODY" and data[1] != "合計":
            es_datas.append({
                "date": target_date,
                "ss": int(data[1]),
                "tv": int(data[8].replace(",", "")),
                "nt": int(data[10].replace(",", "")),
                "op": data[4],
                "hp": data[5],
                "lp": data[6],
                "cp": data[2],
                "nc": data[3][0],
                "gs": data[3][1:]
            })

def get_over_the_counter_data(target_date, db=None):
    rsp = requests.get(f"https://www.tpex.org.tw/web/stock/aftertrading/daily_close_quotes/stk_quote_result.php?l=zh-tw&d={_date_translate(target_date)}")
    if rsp.status_code != 200:
        print("Can't get OTC data")
        return 1

    otc_data = json.loads(rsp.text)
    if otc_data["iTotalRecords"] == 0:
        return 0

    all_datas = []

    for data in otc_data['aaData']:
        all_datas.append({
            "date": target_date,
            "ss": data[0],                          # Stock Symbol (證劵代號)
            "tv": int(data[8].replace(",", "")),    # Trading Volume (成交股數)
            "nt": int(data[10].replace(",", "")),   # Number of Transactions (成交筆數)
            "op": data[4],            # Opening Price (開盤價)
            "hp": data[5],            # Highest Price (最高價)
            "lp": data[6],            # Lowerest Price (最低價)
            "cp": data[2],            # Closing Price (收盤價)
            "nc": data[3][0],                       # Net Change (漲跌)
            "gs": data[3][1:]         # Gross Spread (漲跌價差)
        })

    db.write_data("investment", otc_history_price, all_datas)

def get_stock_exchange_data(target_date, db=None):
    rsp = requests.get(f"https://www.twse.com.tw/exchangeReport/MI_INDEX?date={target_date}&type=ALL")
    if rsp.status_code != 200:
        print(f"Get {target_date} FAILED!!")
        print(rsp.reaseon)
        return 1

    se_data = json.loads(rsp.text)
    if se_data["stat"] != "OK":
        return 1

    if not db:
        db = db_process.mongoDB()
    all_datas = []

    for data in se_data["data9"]:
        all_datas.append({
            "date": target_date,
            "ss": data[0],
            "tv": int(data[2].replace(",", "")),
            "nt": int(data[3].replace(",", "")),
            "op": data[5],
            "hp": data[6],
            "lp": data[7],
            "cp": data[8],
            "nc": "+" if "+" in data[9] else "-",
            "gs": data[10]
        })
    db.write_data("investment", se_history_price, all_datas)

log_process(f"{datetime.today().strftime('%Y%m%d')}.log")
monitoring.register(CommandLogger())

if __name__ == '__main__':
    db = db_process.DBProcess()
    today = datetime.today().strftime("%Y%m%d")
    get_stock_exchange_data(today, db)
    get_over_the_counter_data(today, db)
