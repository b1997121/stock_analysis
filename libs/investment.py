from mongoengine import Document, StringField, IntField


class stock_code(Document):
    code = StringField(require=True)
    name = StringField(require=True)
    ISIN = StringField(require=True)
    listing_date = StringField(require=True)
    market_type = StringField(require=True)
    industry = StringField()
    CFI_code = StringField(require=True)

class otc_history_price(Document):
    date = StringField(require=True)
    ss = StringField(require=True)
    tv = IntField(require=True)
    nt = IntField(require=True)
    op = StringField(require=True)
    hp = StringField(require=True)
    lp = StringField(require=True)
    cp = StringField(require=True)
    nc = StringField(require=True)
    gs = StringField(require=True)

class se_history_price(Document):
    date = StringField(require=True)
    ss = StringField(require=True)
    tv = IntField(require=True)
    nt = IntField(require=True)
    op = StringField(require=True)
    hp = StringField(require=True)
    lp = StringField(require=True)
    cp = StringField(require=True)
    nc = StringField(require=True)
    gs = StringField(require=True)