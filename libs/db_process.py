# -*- encoding: utf8-*-

from mongoengine import connect, disconnect

class DBProcess(object):
    def write_data(self, db_name, collection_name, datas=None):
        if not datas:
            return 0

        print(f"Write {len(datas)} datas to {collection_name}")
        connect(db_name)
        for data in datas:
            collection_name(**data).save()
        disconnect()
